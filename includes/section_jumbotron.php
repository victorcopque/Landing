<section class="landing">
    <div class="container">
        <div class="jumbotron">
            <h1>
                <span class="life">Welcome </span>to my life
            </h1>
            <p class="lead">Esta Landing Page é apenas uma prévia do meu site pessoal. O mesmo conterá não só um blog onde postarei algumas notícias, como também a minha linha do tempo e descrição dos projetos os quais já fiz(ou faço) parte.</p>
            <p>
                <a class="btn btn-lg btn-success" href="https://github.com/victorcopque/PersonalWebsite" target="_blank" role="button">See this project on <span>GitHub</span> <i class="fa fa-github-alt"></i></a>
                <button class="btn btn-lg btn-primary scroll">Contact &Darr;</button>
            </p>
        </div>
    </div>
</section>