<!DOCTYPE html>
<html lang="pt-BR">
  <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Victor Copque</title>
        <!-- Bootstrap -->
        <link href="vendor/css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="resources/css/main.css">
        <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Poiret+One' rel='stylesheet' type='text/css'>
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <header>
            <nav>
                <div class="container-fluid">
                    <div class="navbar-header">
                        VictorCopque
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="navbar-right">
                            <li class="tooltip-inner" data-toggle="tooltip" data-placement="left" title="Please contact me by WhatsApp.<br/>My phone is: (071) 9300-3147"></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>