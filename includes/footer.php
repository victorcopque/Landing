		<footer>
			<div class="row">
				<div class="col-md-6"><h1>Nedd More <span>Coffee!</span></h1></div>
				<div class="col-md-6 col-xs-12">

					<div class="col-md-3 col-xs-6 square-less text-center"><a href="https://github.com/victorcopque" target="_blank"><i class="fa fa-github-alt"></i><p>Encontre-me no GitHub</p></a></div>
					<div class="col-md-3 col-xs-6 square-less text-center"><a href="http://br.linkedin.com/pub/victor-copque/94/134/9a" target="_blank"><i class="fa fa-linkedin-square"></i><p>Veja o meu perfil no Linkedin</p></a></div>
					<div class="col-md-3 col-xs-6 square-less text-center"><a href="https://www.facebook.com/vcopque" target="_blank"><i class="fa fa-facebook-square"></i><p>Siga-me no Facebook</p></a></div>
					<div class="col-md-3 col-xs-6 square-less text-center"><a href="https://plus.google.com/u/0/+VictorCopqueReis/about" target="_blank"><i class="fa fa-google-plus-square"></i><p>Adicione-me aos seus círculos</p></a></div>

					<div class="col-md-12 col-xs-12 form-mobile">
				  		<form class="form-horizontal">
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-2 control-label">Email</label>
								<div class="col-sm-8">
								  <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
								</div>
							</div>
							<div class="form-group">
								<label for="usr" class="col-sm-2 control-label">Name</label>
								<div class="col-sm-8">
								  <input type="text" class="form-control" id="usr" placeholder="Nome">
								</div>
							</div>
							<div class="form-group">
								<label for="areatext" class="col-sm-2 control-label">Message</label>
								<div class="col-sm-8">
								  <textarea class="form-control" id="areatext" rows="3" placeholder="Mensagem"></textarea>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
								  <button type="submit" class="btn btn-success">Send&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-paper-plane-o"></i></button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</footer>
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="vendor/js/bootstrap.min.js"></script>
		<!-- Function to scroll -->
		<script src="resources/js/scroll.js"></script>

		<script>
			$(function () {
	  			$('[data-toggle="tooltip"]').tooltip({html: true});
			});
		</script>
  	</body>
</html>