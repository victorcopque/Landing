jQuery(document).ready(function($){
	// browser window scroll (in pixels) after which the "back to top" link is shown
	var //duration of the top scrolling animation (in ms)
		scroll_top_duration = 500,
		//grab the "back to top" link
		$back_to_top = $('.scroll');

	//smooth scroll to top
	$back_to_top.on('click', function(event){
		event.preventDefault();
		$('body,html').animate({
			scrollTop: $(document).height()-$(window).height()
		 	}, scroll_top_duration
		);
	});

});